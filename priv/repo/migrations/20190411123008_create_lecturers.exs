defmodule School.Repo.Migrations.CreateLecturers do
  use Ecto.Migration

  def change do
    create table(:lecturers) do
      add :name, :string
      add :course_id, references(:courses, on_delete: :delete_all)

      timestamps()
    end

    create index(:lecturers, [:course_id])
  end
end
