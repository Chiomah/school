# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     School.Repo.insert!(%School.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias NimbleCSV.RFC4180, as: CSV
alias School.Repo
alias School.Management.Course
alias School.Management.Student

"priv/course_list.csv"
|> File.read!
|> CSV.parse_string
|> Enum.each(fn [_,code,title,credits,department] ->

  credits= String.to_integer(credits)
  code= String.to_integer(code)
  %Course{code: code, title: title, credits: credits, department: department }

  |> Repo.insert
end)



"priv/student_list.csv"
|> File.read!
|> CSV.parse_string
|> Enum.each(fn [_,name,department] ->


  %Student{name: name, department: department }

  |> Repo.insert
end)
