defmodule SchoolWeb.Acceptance.CoursedetailspageTest do
  use School.DataCase
  use Hound.Helpers
  hound_session()



  test "succesfully navigate to course details page" do

      navigate_to("/course/1")
      page_title = find_element(:css, ".title-text") |> visible_text()
      assert page_title ==  "Course Details"

     

      title = find_element(:css, ".title") |> visible_text()
      code = find_element(:css, ".code") |> visible_text()
      department = find_element(:css, ".department") |> visible_text()

      assert code  == "Code"
      assert title == "Title"
      assert department == "Department"



  end
end
