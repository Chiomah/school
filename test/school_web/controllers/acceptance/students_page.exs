defmodule SchoolWeb.Acceptance.StudentpageTest do
  use School.DataCase
  use Hound.Helpers
  hound_session()



  test "succesfully navigate to students page" do

      navigate_to("/student")

      page_title = find_element(:css, ".title-text") |> visible_text()
      assert page_title == "Students"

      table = find_element(:css, ".table")
      table_name = find_within_element(product, :css, ".name") |> visible_text()
      table_action = find_within_element(product, :css, ".action") |> visible_text()

      assert table_subject  == "Name"
      assert table_action == "Action"



  end
end
