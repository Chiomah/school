defmodule SchoolWeb.Acceptance.CoursepageTest do
  use School.DataCase
  use Hound.Helpers
  hound_session()



  test "succesfully navigate to course page" do

      navigate_to("/course")
      page_title = find_element(:css, ".title-text") |> visible_text()
      assert page_title ==  "Courses"

      table = find_element(:css, ".table")
      table_code = find_within_element(product, :css, ".code") |> visible_text()
      table_title = find_within_element(product, :css, ".title") |> visible_text()
      table_credits = find_within_element(product, :css, ".credits") |> visible_text()
      assert table_code  == "Code"
      assert table_title == "Title"
      assert table_credits == "Credits"



  end
end
