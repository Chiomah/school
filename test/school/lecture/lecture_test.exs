defmodule School.LectureTest do
  use School.DataCase

  alias School.Lecture

  describe "lecturers" do
    alias School.Lecture.Lecturer

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def lecturer_fixture(attrs \\ %{}) do
      {:ok, lecturer} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Lecture.create_lecturer()

      lecturer
    end

end
end
