defmodule School.Lecture.Lecturer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "lecturers" do
    field :name, :string
   
    belongs_to :course, School.Management.Course

    timestamps()
  end

  @doc false
  def changeset(lecturer, attrs) do
    lecturer
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
