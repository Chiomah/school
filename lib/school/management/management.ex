defmodule School.Management do
  import Ecto.Query, warn: false
  alias  School.Management.Student
  alias  School.Management.Course
  alias  School.Management.Request
  alias  School.Repo
  alias School.Lecture
alias School.Lecture.Lecturer


  def list_students do
    Repo.all(Student)
  end
def get_student!(id), do: Repo.get!(Student, id) |> Repo.preload(:courses)


def get_student_by_matno(matno), do: Repo.get_by(Student, matno: matno)



def list_courses do
  Repo.all(Course)
end

def list_chemistry_courses(dept) do
  list_courses()
  |> Enum.filter(fn(course) -> course.department == "Chemistry"
  end)
end

def list_physics_courses(dept) do
  list_courses()
  |> Enum.filter(fn(course) -> course.department == "Physics"
  end)
end


def add_course( cid, %Student{} = student) do
Request.changeset(%Request{}, %{student_id: student.id, course_id: cid})
  |>Repo.insert()

end

def add_course( cid, sid) do
  Request.changeset(%Request{}, %{student_id: sid, course_id: cid})
    |>Repo.insert()

  end

def get_course!(id), do: Repo.get!(Course, id) |> Repo.preload(:students) |> Repo.preload(:lecturers)


def delete_course(%Course{} = course) do
  Repo.delete(course)
end

end
