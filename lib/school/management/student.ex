defmodule School.Management.Student do
  use Ecto.Schema
  import Ecto.Changeset
  alias School.Management.Student
  alias School.Management.Course

  schema "students" do
    field :department, :string
    field :name, :string
    many_to_many(
      :courses,
      Course,
      join_through: "requests",
      on_replace: :delete
)

    timestamps()
  end

  @doc false
  def changeset(student, attrs) do
    student
    |> cast(attrs, [:name, :department])
    |> validate_required([:name, :department])
  end
end
