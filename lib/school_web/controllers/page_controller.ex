defmodule SchoolWeb.PageController do
  use SchoolWeb, :controller

  def index(conn, _params) do
    conn
    |>redirect(to: Routes.course_path(conn, :index))

  end
end
