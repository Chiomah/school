defmodule SchoolWeb.CourseController do
  use SchoolWeb, :controller
alias  School.Management.Student
alias School.Management.Course
alias School.Management
alias School.Lecture
alias School.Lecture.Lecturer

  def index(conn, _params) do
    course = Management.list_courses

   # course2 =  Management.list_physics_courses
    render(conn, "index.html",  course: course  )
  end

  def show(conn, %{"id" => id}) do
   course = Management.get_course!(id)
    lecturer_changeset = Lecture.change_lecturer(%Lecturer{})
    render(conn, "show.html", course: course, lecturer_changeset: lecturer_changeset)
  end

end
