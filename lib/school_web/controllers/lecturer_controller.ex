defmodule SchoolWeb.LecturerController do
  use SchoolWeb, :controller
alias  School.Management.Student
alias School.Management.Course
alias School.Management
alias School.Lecture
alias School.Lecture.Lecturer

  def index(conn, _params) do
    course = Management.list_courses

   # course2 =  Management.list_physics_courses
    render(conn, "index.html",  course: course  )
  end

  def create(conn, %{ "lecturer" => lecturer_params}) do
    course_id = Map.get(lecturer_params, "course_id")
    course = Management.get_course!(course_id)

    case Lecture.create_lecturer(course, lecturer_params) do
      {:ok, _lecturer } ->
        conn
        |> put_flash(:info, " Lecturer created successfully.")
        |> redirect(to: Routes.course_path(conn, :show, course))
      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Issue creating lecturer .")
        |> redirect(to: Routes.course_path(conn, :show, course))
    end
  end

end
