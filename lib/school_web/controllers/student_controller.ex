defmodule SchoolWeb.StudentController do
  use SchoolWeb, :controller
alias  School.Management.Student
alias School.Management.Course
alias School.Management

  def index(conn, _params) do
    student = Management.list_students

   # course2 =  Management.list_physics_courses
    render(conn, "index.html",  student: student )
  end

  def show(conn, %{"id" => id}) do
    course = Management.list_courses
student = Management.get_student!(id)
render(conn, "show.html", student: student, course: course)
end

def create(conn, %{ "course" => course_params}) do
  student = Map.get(course_params, "student_id")
  course_id = Map.get(course_params, "course_id")
  case Management.add_course(course_id, student) do
    {:ok,_} ->

      conn

      |> put_flash(:info, "Course Added successfully.")
      |> redirect(to: Routes.student_path(conn, :index))

    {:error, %Ecto.Changeset{} = changeset} ->
      conn
      |> put_flash(:info, "Memo already  sent to this user.")
      |> redirect(to: Routes.course_path(conn, :index))
   #   render conn, "index.html"
  end
end


end
