# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :school,
  ecto_repos: [School.Repo]

# Configures the endpoint
config :school, SchoolWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "JKDv1a0VIDjaaTVjaHh055Sp7C/JtS9YpuwDg7u6Xck7q4LVv6An3Q5nsFd3jfuJ",
  render_errors: [view: SchoolWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: School.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
